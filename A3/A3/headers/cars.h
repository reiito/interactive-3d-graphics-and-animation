#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"
#include "animation.h"

#define CAR_AMT 4
#define CAR_MESH 2
#define CAR_MAT 3

   typedef struct Car Car;
   typedef struct Mesh Mesh;
   typedef struct Material Material;
   typedef struct MaterialContainer MaterialContainer;
   typedef struct DebugControls DebugControls;

   struct Car
   {
      vec3f min;
      vec3f max;

      vec3f pos[CAR_AMT];
      vec3f vel;
      float size;

      Mesh *meshes[CAR_MESH];
      Material *materials[CAR_MAT];
   };

   void initCars(Car *car, MaterialContainer *container, Mesh *meshes[],
      vec3f vel, float size, vec3f min, vec3f max);

   void renderCars(Car *car, IdleAnimation *idleAnimation,
      DebugControls *controls);

   void updateCars(Car *car, IdleAnimation *idleAnimation, float dt);

   void drawTop(Mesh *mesh, Material *materials[], DebugControls *controls);
   void drawWheels(Mesh *mesh, IdleAnimation *idleAnimation,
      Material *materials[], DebugControls *controls);
   void drawFront(Mesh *mesh, Material *materials[], DebugControls *controls);
   void drawBack(Mesh *cube, Mesh *cylinder, Material *materials[],
      DebugControls *controls);

#if __cplusplus
}
#endif

