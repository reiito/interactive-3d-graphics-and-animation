#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"
#include "gl.h"

#define LOG_AMT 4

   typedef struct Log Log;
   typedef struct Mesh Mesh;
   typedef struct Material Material;
   typedef struct MaterialContainer MaterialContainer;
   typedef struct DebugControls DebugControls;

   struct Log
   {
      vec3f min;
      vec3f max;

      vec3f vel;
      float size;

      vec3f pos[LOG_AMT];

      GLuint tex;

      Mesh *mesh;
      Material *material;
   };

   void initLogs(Log *log, MaterialContainer *container, Mesh *mesh, GLuint tex,
      vec3f vel, float size, vec3f min, vec3f max);
   void renderLogs(Log *log, DebugControls *controls);

   void updateLogs(Log *log, float dt);

#if __cplusplus
}
#endif

