#include "../headers/cars.h"
#include "../headers/mesh.h"
#include "../headers/controls.h"
#include "../headers/material.h"
#include "../headers/utils.h"
#include "../headers/gl.h"

void initCars(Car *car, MaterialContainer *container, Mesh *meshes[], vec3f vel,
   float size, vec3f min, vec3f max)
{
   // boundries
   car->min = min;
   car->max = max;

   // set positions bsed on amount of cars (lanes)
   float inc = ((max.z - min.z) / (CAR_AMT - 1));
   float z = max.z;
   for (size_t i = 0; i < CAR_AMT; i++)
   {
      car->pos[i] = cVec3f(getTRand(min.x, max.x), min.y, z);
      z -= inc;
   }

   car->vel = vel;
   car->size = size;

   // assign car meshes
   car->meshes[0] = meshes[0]; //cube
   car->meshes[1] = meshes[1]; //cylinder

   // assign materials
   car->materials[0] = getMaterial(container, "car");
   car->materials[1] = getMaterial(container, "wheel");
   car->materials[2] = getMaterial(container, "white");
}

void renderCars(Car *car, IdleAnimation *idleAnimation, DebugControls *controls)
{
   glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);
   glDisable(GL_TEXTURE_2D);

   for (size_t i = 0; i < CAR_AMT; i++) //render given amount of cars
   {
      // [1] draw car
      glPushMatrix();
         glTranslatef(car->pos[i].x, car->pos[i].y + 0.25, car->pos[i].z); //pos
         glRotatef(90.0, 0, 1, 0); //rotate to face left
         glScalef(car->size, car->size, car->size); //car size
         renderMesh(car->meshes[0], car->materials[0], cVec3f(1, 0.35, 1.5),
            controls); //body
         drawTop(car->meshes[0], car->materials, controls);
         drawWheels(car->meshes[1], idleAnimation, car->materials, controls);
         drawFront(car->meshes[0], car->materials, controls);
         drawBack(car->meshes[0], car->meshes[1], car->materials, controls);
      glPopMatrix(); //[1]
   }

   glPopAttrib();
}

void updateCars(Car *car, IdleAnimation *idleAnimation, float dt)
{
   for (size_t i = 0; i < CAR_AMT; i++)
   {
      car->pos[i].y += car->vel.y * dt;
      car->pos[i].x += car->vel.x * dt;
      car->pos[i].z += car->vel.z * dt;

      if (car->pos[i].x > car->max.x)
         car->pos[i].x = car->min.x;
      if (car->pos[i].y > car->max.y)
         car->pos[i].y = car->min.y;
      if (car->pos[i].z > car->max.z)
         car->pos[i].z = car->min.z;
   }

   // add wheel animation
   interpolate(&idleAnimation->wheels, dt);
}

void drawTop(Mesh *mesh, Material *materials[], DebugControls *controls)
{
   // sizes + window color
   vec3f top = cVec3f(1.0, 0.35, 1.0);
   vec3f mirror = cVec3f(0.15, 0.15, 0.05);
   vec3f window = cVec3f(1.0, 0.25, 0.05);
   vec3f spoiler = cVec3f(1.0, 0.05, 0.2);
   vec3f pos = cVec3f(0, 0.35, -0.15);

   // [1] draw car top
   glPushMatrix();
      glTranslatef(pos.x, pos.y + top.y, pos.z);
      renderMesh(mesh, materials[0], top, controls);

      // [2] side Mirrors
      glPushMatrix();
         // [a] left mirror
         glTranslatef(-top.x - mirror.x, -top.y + mirror.y, top.z - mirror.z);
         renderMesh(mesh, materials[0], mirror, controls);
         // Move back to middle
         glTranslatef(top.x + mirror.x, 0.0, 0.0);
         // [b] right mirror
         glTranslatef(top.x + mirror.x, 0.0, 0.0);
         renderMesh(mesh, materials[0], mirror, controls);
      glPopMatrix(); //[2]

      // [3] front/back window + rear bonnet
      glPushMatrix();
         // [a] front window
         glTranslatef(0.0, -(top.y - window.y), top.z + window.z);
         renderMesh(mesh, materials[2], window, controls);
         // move back to middle
         glTranslatef(0.0, (top.y - window.y), -top.z - window.z);
         // [b] back window
         glTranslatef(0.0, 0.0, -top.z - window.z);
         renderMesh(mesh, materials[2], window, controls);
         // [c] spoiler
         glTranslatef(0.0, -window.y - spoiler.y, -spoiler.z + window.z);
         renderMesh(mesh, materials[0], spoiler, controls);
      glPopMatrix(); //[3]
   glPopMatrix(); //[1]
}

void drawWheels(Mesh *mesh, IdleAnimation *idleAnimation, Material *materials[],
   DebugControls *controls)
{
   vec3f tyreSize = cVec3f(0.4, 0.4, 0.25); //rubber tyre
   vec3f plateSize = cVec3f(0.2, 0.2, 0.1); //wheel plate
   vec3f pos = cVec3f(1.0, -0.25, 1.15);

   // [1] draw wheels (tyre + wheelPlate)
   glPushMatrix();
      glRotatef(-90, 0, 1, 0); //rotate back

      // [2] front left wheel
      glPushMatrix();
         // [a] tyre
         glTranslatef(pos.x, pos.y, -pos.z);
         glRotatef(idleAnimation->wheels.rotation, 0, 0, 1);
         renderMesh(mesh, materials[1], tyreSize, controls);
         // [b] rim
         glTranslatef(0, 0, -plateSize.z);
         renderMesh(mesh, materials[2], plateSize, controls);
      glPopMatrix(); //[2]

      // [3] front right wheel
      glPushMatrix();
         // [a] tyre
         glTranslatef(pos.x, pos.y, pos.z);
         glRotatef(idleAnimation->wheels.rotation, 0, 0, 1);
         renderMesh(mesh, materials[1], tyreSize, controls);
         // [b] rim
         glTranslatef(0, 0, plateSize.y);
         renderMesh(mesh, materials[2], plateSize, controls);
      glPopMatrix(); //[3]

      // [4] back left wheel
      glPushMatrix();
         // [a] tyre
         glTranslatef(-pos.x, pos.y, -pos.z);
         glRotatef(idleAnimation->wheels.rotation, 0, 0, 1);
         renderMesh(mesh, materials[1], tyreSize, controls);
         // [b] rim
         glTranslatef(0, 0, -plateSize.z);
         renderMesh(mesh, materials[2], plateSize, controls);
      glPopMatrix(); //[4]

      // [5] back right wheel
      glPushMatrix();
         // [a] tyre
         glTranslatef(-pos.x, pos.y, pos.z);
         glRotatef(idleAnimation->wheels.rotation, 0, 0, 1);
         renderMesh(mesh, materials[1], tyreSize, controls);
         // [b] rim
         glTranslatef(0, 0, plateSize.y);
         renderMesh(mesh, materials[2], plateSize, controls);
      glPopMatrix(); //[5]
   glPopMatrix(); //[1]
}

void drawFront(Mesh *mesh, Material *materials[], DebugControls *controls)
{
   // Sizes and colors
   vec3f front = cVec3f(1.0, 0.25, 0.1);
   vec3f hLight = cVec3f(0.15, 0.15, 0.05);
   vec3f bumper = cVec3f(0.55, 0.2, 0.05);
   vec3f pos = cVec3f(0.0, 0.0, 1.5);

   // [1] draw car front + headlights + bumper
   glPushMatrix();
      // [a] bumper
      glTranslatef(pos.x, pos.y, pos.z + front.z);
      renderMesh(mesh, materials[1], front, controls);

      // [b] left headlight
      glTranslatef(-front.x + hLight.x + hLight.x, 0.0, front.z + hLight.z);
      renderMesh(mesh, materials[2], hLight, controls);

      glTranslatef(hLight.x + bumper.x, 0.0, 0.0); //headlight space

      // [c] right headlight
      glTranslatef(hLight.x + bumper.x, 0.0, 0.0);
      renderMesh(mesh, materials[2], hLight, controls);
   glPopMatrix(); //[1]
}

// [Car] - Comprised of Front, Back, Top, Wheels
void drawBack(Mesh *cube, Mesh *cylinder, Material *materials[],
   DebugControls *controls)
{
   // Sizes and Colors
   vec3f bumper = cVec3f(1, 0.25, 0.05);
   vec3f exhaust = cVec3f(0.1, 0.1, 0.1);
   vec3f tLight = cVec3f(0.25, 0.1, 0.025);
   vec3f pos = cVec3f(0, 0, -1.5);

   // [1] draw back bumper + tailights
   glPushMatrix();
      glTranslatef(pos.x, pos.y, pos.z - bumper.z);
      renderMesh(cube, materials[1], bumper, controls);

      // [2] exhaust
      glPushMatrix();
         glTranslatef(0, -bumper.y, -bumper.z);

         // [3] left pipe
         glPushMatrix();
            glTranslatef(exhaust.y, 0.0, 0.0);
            renderMesh(cylinder, materials[1], exhaust, controls);
         glPopMatrix(); //[3]

         // [4] right pipe
         glPushMatrix();
            glTranslatef(-exhaust.y, 0.0, 0.0);
            renderMesh(cylinder, materials[1], exhaust, controls);
         glPopMatrix(); //[4]
      glPopMatrix(); //[2]

      // [a] left tail light
      glTranslatef(-bumper.x + tLight.x + 0.1, bumper.y - (2.0 * tLight.y),
         -bumper.z - tLight.z);
      renderMesh(cube, materials[2], tLight, controls);
      // [b] right tailight
      glTranslatef((bumper.x + tLight.x + 0.1), 0.0, 0.0);
      renderMesh(cube, materials[2], tLight, controls);
   glPopMatrix(); //[1]
}