#include "../headers/logs.h"
#include "../headers/mesh.h"
#include "../headers/controls.h"
#include "../headers/material.h"
#include "../headers/gl.h"
#include "../headers/utils.h"

void initLogs(Log *log, MaterialContainer *container, Mesh *mesh, GLuint tex,
   vec3f vel, float size, vec3f min, vec3f max)
{
   // boundries
   log->min = min;
   log->max = max;

   log->vel = vel;
   log->size = size;

   // set positions bsed on amount of cars (lanes)
   float inc = ((max.z - min.z) / (LOG_AMT - 1));
   float z = max.z;
   for (size_t i = 0; i < LOG_AMT; i++)
   {
      log->pos[i] = cVec3f(getTRand(min.x, max.x), min.y, z);
      z -= inc;
   }

   log->tex = tex;

   log->mesh = mesh;
   log->material = getMaterial(container, "white");
}

void renderLogs(Log *log, DebugControls *controls)
{
   glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT | GL_TEXTURE_BIT);

   glBindTexture(GL_TEXTURE_2D, log->tex); //apply texture

   for (size_t i = 0; i < LOG_AMT; i++)
   {
      // [1] draw logs
      glPushMatrix();
         glTranslatef(log->pos[i].x, log->pos[i].y, log->pos[i].z); //log pos
         glRotatef(90, 0, 1, 0); //rotate to face left
         glScalef(log->size, log->size, log->size); //log size
         renderMesh(log->mesh, log->material, cVec3f(1,1,10), controls);
      glPopMatrix(); //[1]
   }

   glBindTexture(GL_TEXTURE_2D, 0); //remove textring from others

   glPopAttrib();
}

void updateLogs(Log *log, float dt)
{
   for (size_t i = 0; i < LOG_AMT; i++)
   {
      log->pos[i].y += log->vel.y * dt;
      log->pos[i].x += log->vel.x * dt;
      log->pos[i].z += log->vel.z * dt;

      if (log->pos[i].x > log->max.x)
         log->pos[i].x = log->min.x;
      if (log->pos[i].y > log->max.y)
         log->pos[i].y = log->min.y;
      if (log->pos[i].z > log->max.z)
         log->pos[i].z = log->min.z;
   }
}

