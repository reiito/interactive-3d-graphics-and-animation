#include "../headers/terrain.h"
#include "../headers/mesh.h"
#include "../headers/material.h"
#include "../headers/gl.h"

void initTerrain(Terrain *terrain, MaterialContainer *container, 
   const char *materialName, GLuint tex, vec3f pos, float width, float height,
   size_t rows, size_t cols)
{
   terrain->pos = pos;
   terrain->width = width;
   terrain->height = height;
   terrain->rows = rows;
   terrain->cols = cols;

   terrain->tex = tex;

   terrain->mesh = makeGrid(width, height, rows, cols);
   terrain->material = getMaterial(container, materialName);
}

void renderTerrain(Terrain *terrain, DebugControls *controls)
{
   glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);

   glBindTexture(GL_TEXTURE_2D, terrain->tex);

   // [1] draw grid
   glPushMatrix();
      glTranslatef(terrain->pos.x, terrain->pos.y, terrain->pos.z);
      renderMesh(terrain->mesh, terrain->material, cVec3f(1,1,1), controls);
   glPopMatrix(); //[1]

   glPopAttrib();
}

