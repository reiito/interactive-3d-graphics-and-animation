#include "../headers/globals.h"
#include "../headers/gl.h"

void initGlobals(Globals *globals)
{
   // controls
   initPlayerControls(&globals->playerControls);
   initCameraControls(&globals->cameraControls);
   initDebugControls(&globals->debugControls);

   // meshes
   globals->meshes[0] = makeCube();
   globals->meshes[1] = makeCylinder();
   globals->meshes[2] = makeSphere();

   // materials
   initMaterials(&globals->materials);
   const char terrainMat[STR_LEN] = "terrain";
   const char roadMat[STR_LEN] = "road";
   const char waterMat[STR_LEN] = "water";

   // lighting
   initLight(&globals->light,
      cVec4f(100, 100, -100, 0),
      cVec4f(0.2f, 0.2f, 0.2f, 1),
      cVec4f(1, 1, 1, 1),
      cVec4f(0.1f, 0.1f, 0.1f, 1));

   // load textures
   GLuint grassTexture = loadTexture("resources/grass.jpg");
   GLuint roadTexture = loadTexture("resources/road.png");
   GLuint riverTexture = loadTexture("resources/sand.jpg");
   GLuint woodTexture = loadTexture("resources/wood.jpg");

   // flat surfaces (grids)
   initTerrain(&globals->terrains[0], &globals->materials, terrainMat,
      grassTexture, cVec3f(0, 0, 0), 50, 50, 8, 8);
   initTerrain(&globals->terrains[1], &globals->materials, roadMat, roadTexture,
      cVec3f(0, 0.01f, -12), 50, 20, 8, 8);
   initTerrain(&globals->terrains[2], &globals->materials, waterMat, riverTexture,
      cVec3f(0, 0.1f, 12), 50, 20, 8, 8);

   // player details
   initPlayer(&globals->player, &globals->materials, globals->meshes[0],
      cVec3f(0, 0, -23.5), 0.25, 90, 45, 5);
   initParabola(&globals->parabola, &globals->player);
   attachCameraToPlayer(&globals->camera, &globals->player);

   // cars + logs
   initCars(&globals->cars, &globals->materials, globals->meshes,
      cVec3f(4, 0, 0), 1, cVec3f(-25, 0.5, -19.5), cVec3f(25, 0, -4.5));
   initLogs(&globals->logs, &globals->materials, globals->meshes[1],
      woodTexture, cVec3f(4, 0, 0), 1, cVec3f(-25, 0, 4.5),
      cVec3f(25, 0, 19.5));

   // skybox
   //initSkybox(&globals->skybox, globals->meshes[0], &globals->materials,
   //   skyboxTexture, cVec3f(0,0,0), cVec3f(75,75,75));

   // animations
   initAnimation(&globals->animation);
   initIdleAnimation(&globals->idleAnimation);

   // frame rate
   globals->frames = 0;
   globals->frameRate = 0;
   globals->frameRateInterval = 0.2;
   globals->lastFrameRateT = 0;

   initRendering();
}

void freeGlobals(Globals *globals)
{
   // clear all stored meshes
   for (int i = 0; i < GLOBAL_MESH; i++)
      dMesh(globals->meshes[i]);

   // clear planes
   for (int i = 0; i < TERRAIN_AMT; i++)
      dMesh(globals->terrains[i].mesh);

   // clear materials
   dMaterialContainer(&globals->materials);
}

