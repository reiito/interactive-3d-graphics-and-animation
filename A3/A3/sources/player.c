#include "../headers/player.h"
#include "../headers/mesh.h"
#include "../headers/controls.h"
#include "../headers/material.h"
#include "../headers/gl.h"

/* STATIC FUNCIONS */
static void renderDir(Player *player)
{
   static float scale = 0.5f;

   // If we're not moving, draw the direction vector for the start of the jump,
   // otherwise draw current velocity.
   // This is a *little* bit hacky and you don't need to do it, but I wanted to
   // see the velocity change during the jump.
   vec3f dir = mulVec3f(player->forward, player->jumpSpeed);
   if (player->vel.x != 0 || player->vel.y != 0)
      dir = player->vel;

   glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
   glDisable(GL_LIGHTING);
   glDisable(GL_TEXTURE_2D);

   // You could use the player's translation/rotation to handle this instead of
   // using pos/dir.
   glColor3f(1, 0, 1);
   glBegin(GL_LINES);
   glVertex3f(player->pos.x, player->pos.y, player->pos.z);
   glVertex3f(player->pos.x + dir.x * scale,
      player->pos.y + dir.y * scale,
      player->pos.z + dir.z * scale);
   glEnd();

   glPopAttrib();
}

static void updateControls(Player *player, PlayerControls *controls, float dt)
{
   // This stuff used to be in an update function in globals.c, but I've moved
   // it here. It's purely for setting the values of heading/jumpRot/jumpSpeed
   // based on what keys are being pressed.
   static float rotSpeed = 45.0f * M_PI / 180.0f;
   static float speedSpeed = 10.0f;

   // I figured I'd let the player turn while in the air. This isn't that
   // noticeable with a sphere, but it's much easier to see when you add a
   // frog model.
   float turn = 0;
   if (controls->left)
      turn += player->turnSpeed;
   if (controls->right)
      turn -= player->turnSpeed;
   player->heading += turn *dt;

   // Don't change rot/speed if we're already jumping.
   if (player->jumpFlag)
      return;

   float rot = 0;
   float speed = 0;

   if (controls->downRot && player->jumpRot >= MIN_ROT)
      rot -= rotSpeed * dt;
   if (controls->upRot && player->jumpRot <= MAX_ROT)
      rot += rotSpeed * dt;
   if (controls->upSpeed && player->jumpSpeed <= MAX_SPD)
      speed += speedSpeed * dt;
   if (controls->downSpeed && player->jumpSpeed >= MIN_SPD)
      speed -= speedSpeed * dt;

   player->jumpRot += rot;
   player->jumpSpeed += speed;
}

static void updatePlayerPos(Player *player, Animation *animation, float dt)
{
   // We only use numerical integration now since it's much simpler and can be
   // used to handle any kind of movement
   // (not just jumping).
   static float gravity = 9.8f;

   // If we've reached the end of our jump (or aren't jumping), don't integrate.
   if (player->vel.y <= 0 && !player->jumpFlag)
      return;

   player->vel.y -= gravity * dt;

   player->pos.y += player->vel.y * dt + 0.5f * gravity * dt * dt;
   player->pos.x += player->vel.x * dt + 0.5f * dt * dt;
   player->pos.z += player->vel.z * dt + 0.5f * dt * dt;

   jumpAnimation(animation, dt);
}

static void orientPlayer(Player *player)
{
   // Only calculate a new forward vector if not jumping.
   if (player->jumpFlag)
      return;

   player->forward.x = sinf(player->heading * (float)M_PI / 180.0f)
      * cosf(player->jumpRot);
   player->forward.z = cosf(player->heading * (float)M_PI / 180.0f)
      * cosf(player->jumpRot);
   player->forward.y = sinf(player->jumpRot);
}


/* REAL FUNCTIONS */

vec3f initPos;
float initJumpRot;
float initJumpSpeed;

void initPlayer(Player *player, MaterialContainer *container, Mesh *mesh,
   vec3f pos, float size, float turnSpeed, float jumpRot, float jumpSpeed)
{
   initPos = pos;
   player->pos = initPos;
   player->vel = cVec3f(0, 0, 0);
   player->size = size;
   player->heading = 0;

   player->forward = cVec3f(0, 0, 1);
   
   player->turnSpeed = turnSpeed;

   initJumpRot = jumpRot;
   player->jumpRot = initJumpRot;
   initJumpSpeed = jumpSpeed;
   player->jumpSpeed = initJumpSpeed;
   
   player->lives = LIVES_AMT;
   player->score = 0;
   player->end = false;

   player->numDead = 0;
   for (int i = 0; i < LIVES_AMT; i++)
   {
      player->deadPlayer[i].heading = 0;
      player->deadPlayer[i].pos = (vec3f) { 0, 0, 0 };
   }

   player->mesh = mesh;
   player->materials[0] = getMaterial(container, "green");
   player->materials[1] = getMaterial(container, "white");
   player->materials[2] = getMaterial(container, "black");
}

void resetPlayer(Player *player, bool died)
{
   int numDied = player->numDead;
   player->deadPlayer[numDied].pos = player->pos;
   player->deadPlayer[numDied].heading = player->heading;
   player->deadPlayer[numDied].on = player->on;
   player->numDead++;
   if (!died)
      player->score++;
   else
      player->lives--;

   if (player->lives == 0 || player->score == WIN_SCORE) //lose condition
      player->end = true;
   else
   {
      player->pos = initPos;
      player->vel = cVec3f(0, 0, 0);
      player->heading = 0;
      player->jumpRot = initJumpRot;
      player->jumpSpeed = initJumpSpeed;
   }
}

void renderPlayer(Player *player, Animation *animation,
   IdleAnimation *idleAnimation, DebugControls *controls)
{
   glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);
   glDisable(GL_TEXTURE_2D);

   glPushMatrix();
      glTranslatef(player->pos.x, player->pos.y + 0.55, player->pos.z);
      glRotatef(player->heading, 0, 1, 0);
      glScalef(player->size, player->size, player->size);
      glPushMatrix();
         glRotatef(animation->body.rotation, 1, 0, 0);
         renderMesh(player->mesh, player->materials[0], cVec3f(1.5, 1.5, 2.5),
            controls);
         drawHead(player->mesh, idleAnimation, player->materials, controls);
         drawArm(player->mesh, animation, player->materials, true, controls);
         drawArm(player->mesh, animation, player->materials, false, controls);
         drawLeg(player->mesh, animation, player->materials, true, controls);
         drawLeg(player->mesh, animation, player->materials, false, controls);
      glPopMatrix();
   glPopMatrix();

   glPopAttrib();

   renderDir(player);
}

void renderDead(Player *player, vec3f pos, float rotation,
   DebugControls *controls)
{
   // Create dummy values
   Animation *animation = NULL;
   IdleAnimation *idleAnimation = NULL;

   glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);
   glDisable(GL_TEXTURE_2D);

   glPushMatrix();
      glTranslatef(pos.x, pos.y + 0.125, pos.z);
      glRotatef(rotation, 0, 1, 0);
      glScalef(player->size, 0.001f, player->size); //flatten
      glPushMatrix();
         renderMesh(player->mesh, player->materials[0], cVec3f(1.5, 1.5, 2.5),
            controls);
         drawHead(player->mesh, idleAnimation, player->materials, controls);
         drawArm(player->mesh, animation, player->materials, true, controls);
         drawArm(player->mesh, animation, player->materials, false, controls);
         drawLeg(player->mesh, animation, player->materials, true, controls);
         drawLeg(player->mesh, animation, player->materials, false, controls);
      glPopMatrix();
   glPopMatrix();

   glPopAttrib();
   renderDir(player);
}

void renderAllDead(Player *player, DebugControls *controls)
{
   vec3f diedPos;
   float diedDir;

   for (int i = 0; i < player->numDead; i++)
   {
      if (player->deadPlayer[i].on == WATER
         || player->deadPlayer[i].on == NOTHING)
         break;
      //ensure the frog corpose isn't in the sky
      player->deadPlayer[i].pos.y = 0.0;
      renderDead(player, player->deadPlayer[i].pos,
         player->deadPlayer[i].heading, controls);
   }
}

void updatePlayer(Player *player, Animation *animation,
   IdleAnimation *idleAnimation, PlayerControls *controls, float dt)
{
   updateControls(player, controls, dt);
   updatePlayerPos(player, animation, dt);
   orientPlayer(player);
   ribbitAnimation(idleAnimation, dt);

   // Finished the jump? Reset everything.
   if (!player->jumpFlag)
   {
      initAnimation(animation); // Reset jump animation
   }
}

void updatePlayerCollisions(Player *player, Car *cars, Log *logs)
{
   int ground = 0;
   int log = 0;

   if (player->on == NOTHING)
      ground = -10;
   else if (player->on == ROAD)
      ground = 0.1;
   else if (player->on == WATER)
      ground = 0.1;
   else if (player->on == LOG)
   {
      player->pos.x = logs->pos[log].x;
      ground == 1;
   }
   else if (player->on == GRASS);
      ground = 0;

   if (player->pos.z >= -25 && player->pos.z <= -22.5
      || player->pos.z >= -2.5 && player->pos.z <= 2.5
      || player->pos.z >= -22.5 && player->pos.z <= -2.5) //start + mid grass
   {
      player->on = GRASS;
      if (player->pos.y <= ground)
      {
         player->vel = cVec3f(0, 0, 0);
         player->pos.y = ground;
         player->jumpFlag = false;
      }
   }
   else if (player->pos.z >= 22.5 && player->pos.z <= 25) //end grass
   {
      player->on = GRASS;
      if (player->pos.y <= ground)
      {
         resetPlayer(player, false);
         player->jumpFlag = false;
      }
   }
   else if (player->pos.z >= 2.5 && player->pos.z <= 22.5) //water
   {
      player->on = WATER;
      if (player->pos.y <= ground)
      {
         resetPlayer(player, true);
         player->jumpFlag = false;
      }
   }
   else if (player->pos.y <= 0.1 && player->pos.z >= -22.5 && player->pos.z <= -2.5) //road
   {
      player->on = ROAD;
      if (player->pos.y <= ground)
      {
         player->vel = cVec3f(0, 0, 0);
         player->pos.y = ground;
         player->jumpFlag = false;
      }
   }
   else //off map
   {
      player->on = NOTHING;
      if (player->pos.y <= ground)
      {
         resetPlayer(player, true);
         player->jumpFlag = false;
      }
   }

   // car collider
   for (int i = 0; i < CAR_AMT; i++)
   {
      float xDis = player->pos.x - cars->pos[i].x;
      float zDisMin = player->pos.z - cars->pos[i].z
         + cars->size + 1.5;
      float zDisMax = player->pos.z - cars->pos[i].z
         - cars->size - 1.5;
      float radiusSum = player->size + 1.5 + cars->size;

      if (xDis <= radiusSum && xDis >= 0 && zDisMin >= 0 && zDisMax <= 0)
      {
         resetPlayer(player, true);
      }
   }

   // log collider
   for (int i = 0; i < LOG_AMT; i++)
   {
      float xDis = player->pos.x - logs->pos[i].x;
      float yDis = player->pos.y - logs->pos[i].y;
      float zDisMin = player->pos.z - logs->pos[i].z
         + logs->size;
      float zDisMax = player->pos.z - logs->pos[i].z
         - logs->size;
      float radiusSum = player->size + 1.5 + logs->size;

      if (xDis <= radiusSum && xDis >= 0 && yDis >= 0 && zDisMin >= 0
         && zDisMax <= 0)
      {
         log = i;
         player->on = LOG;
      }
   }
}

void setAnimDuration(Player *player, Animation *animation)
{
   // Calculate time of flight
   double gravity = 9.8;
   float tof = (2.0f * player->jumpSpeed * sinf(player->jumpRot)) / gravity;
   // Set how long animation will run for (jumping further = longer animation)
   animation->body.duration = tof;
   animation->shoulder.duration = tof;
   animation->elbow.duration = tof;
   animation->upperLeg.duration = tof;
   animation->middleLeg.duration = tof;
   animation->lowerLeg.duration = tof;
}

void jump(Player *player, Animation *animation)
{
   // Already jumping? Well sadly double jump is not implemented.
   if (player->jumpFlag)
      return;

   player->vel = mulVec3f(player->forward, player->jumpSpeed);
   player->on = NOTHING;
   player->jumpFlag = true;
   setAnimDuration(player, animation);
}

// [Frog]: Comprised of torso, head (eyes and mouth), arms and legs
void drawHead(Mesh *mesh, IdleAnimation *idleAnimation,
   Material *materials[], DebugControls *controls)
{
   // Scaling size of each individual part, Format = { }
   vec3f head = cVec3f(1.25, 1.25, 0.4);
   vec3f uMouth = cVec3f(1.25, 0.3, 0.75);
   vec3f lMouth = cVec3f(1.25, 0.3, 0.75);
   vec3f eye = cVec3f(0.55, 0.55, 0.25);
   vec3f pupil = cVec3f(0.4, 0.4, 0.05);

   float mouthAnimation = 0;

   if (idleAnimation)
   {  
      // Apply animations only to non-crushed frog (player frog)
      mouthAnimation = idleAnimation->mouth.rotation;
   }

   // [1]. Start at neck joint
   glPushMatrix();
      glTranslatef(0.0, 0.15, 2.85);

      // [2]. Draw back of head
      glPushMatrix();
         renderMesh(mesh, materials[0], head, controls);

         // [3]a. Translate from back head down and forward to draw upper mouth
         glPushMatrix();
            glRotatef(-mouthAnimation, 1, 0, 0); // Add rotation animation
            glTranslatef(0.0, -uMouth.y, head.z + uMouth.z);
            renderMesh(mesh, materials[0], uMouth, controls);
         glPopMatrix();  // Pop back to head pos

         // [3]b. Translate from back head to end of upper mouth to draw 
         // lower mouth
         glPushMatrix();
            // Move to lower mouth joint
            glTranslatef(0.0, -uMouth.y - uMouth.y, 0.0);
            glRotatef(mouthAnimation, 1, 0, 0); // Add rotation animation
            glTranslatef(0.0, -lMouth.y, head.z + lMouth.z); // Draw lower mouth
            renderMesh(mesh, materials[0], lMouth, controls);
         glPopMatrix(); // Pop back to back head pos

         // [4]a. From back head translate up, left and forward to draw left eye
         glPushMatrix();
            glTranslatef(-0.65, 0.65, head.z + eye.z);
            renderMesh(mesh, materials[1], eye, controls);

            // [4]b. From left eye translate forward to draw left pupil
            glPushMatrix();
               glTranslatef(0.0, 0.0, eye.z);
               renderMesh(mesh, materials[2], pupil, controls);
            glPopMatrix(); // Pop back to left eye pos

            // [5]a. From left eye translate right to draw right eye
            glPushMatrix();
               glTranslatef(1.25, 0.0, 0.0);
               renderMesh(mesh, materials[1], eye, controls);

               // [5]b. From right eye translate forward to draw right pupil
               glPushMatrix();
                  glTranslatef(0.0, 0.0, eye.z);
                  renderMesh(mesh, materials[2], pupil, controls);
               glPopMatrix(); // Pop when done with right pupil

            // Done with head pop rest of pushed matricies
            glPopMatrix(); // right eye
         glPopMatrix(); // left eye
      glPopMatrix(); // back of head
   glPopMatrix(); // neck
}

void drawLeg(Mesh *mesh, Animation *animation, Material *materials[], bool left,
   DebugControls *controls)
{
   // Size scaling, Format = { wide, long, tall }
   vec3f legSegment = cVec3f(0.45, 0.95, 0.45);
   vec3f foot = cVec3f(0.5, 0.55, 0.5);
   vec3f toe = cVec3f(0.15, 0.35, 0.15);
   vec3f begin = cVec3f(1.65, -0.5, -2.0);

   float legRotation = 40.0;
   float upperLegRotation = -95.0;
   float middleLegRotation = 145;
   float lowerLegRotation = -135;
   float leftToeRotation = -25;
   float rightToeRotation = 25;

   float upperLegAnimation = 0;
   float middleLegAnimation = 0;
   float lowerLegAnimation = 0;

   if (animation)
   {
      // Apply animations only to non-crushed frog (player frog)
      upperLegAnimation = animation->upperLeg.rotation;
      middleLegAnimation = animation->middleLeg.rotation;
      lowerLegAnimation = animation->lowerLeg.rotation;
   }

   // Rotation based left or right leg (if left = negative, right = default)
   if (left)
   {
      begin = cVec3f(-1.65, -0.5, -2.0);
      legRotation = -legRotation;
   }

   // [1]. Translate to starting upper leg joint
   glPushMatrix();
      glTranslatef(begin.x, begin.y, begin.z); // Move to upper leg joint
      glRotatef(legRotation, 0, 1, 0); // Rotate entire leg outwards
      glRotatef(upperLegRotation, 1, 0, 0); // Rotate upper leg forwards
      glRotatef(upperLegAnimation, 1, 0, 0); // Add rotation animation
      glTranslatef(0.0, -legSegment.y, 0.0); // Move to center of upper leg
      renderMesh(mesh, materials[0], legSegment, controls); // Draw upper leg

      // [2]. From upper leg translate down to upper/mid leg joint
      glPushMatrix();
         glTranslatef(0.0, -legSegment.y, 0.0); // Move to upper/mid leg joint
         glRotatef(middleLegRotation, 1, 0, 0); // Rotate mid leg backwards
         glRotatef(middleLegAnimation, 1, 0, 0); // Add rotation animation
         glTranslatef(0.0, -legSegment.y, 0.0); // Move to center of middle leg
         renderMesh(mesh, materials[0], legSegment, controls); //Draw middle leg

         // [3]. From mid leg translate down to mid/lower leg joint
         glPushMatrix();
            glTranslatef(0.0, -legSegment.y, 0.0); //Move to mid/lower leg joint
            glRotatef(lowerLegRotation, 1, 0, 0); // Rotate lower leg forwards
            glRotatef(lowerLegAnimation, 1, 0, 0); // Add rotation animation
            glTranslatef(0.0, -legSegment.y, 0.0); //Move to center of lower leg
            // Draw lower leg
            renderMesh(mesh, materials[0], legSegment, controls);

            // [4]. From lower leg translate to ankle joint
            glPushMatrix();
               glTranslatef(0.0, -legSegment.y, 0.0); // Move to ankle
               glTranslatef(0.0, -foot.y, 0.0); // Move to center of foot
               renderMesh(mesh, materials[0], foot, controls); // Draw foot
               glTranslatef(0.0, -foot.y, 0.0); // Move to end of foot

               // [5]a. From foot translate down and left to draw left toe
               glPushMatrix();
                  glTranslatef(-0.45, -toe.y, 0.0);
                  glRotatef(leftToeRotation, 0, 0, 1);
                  renderMesh(mesh, materials[0], toe, controls);
               glPopMatrix(); // Pop back to foot position

               // [5]b. From foot translate down to draw middle toe
               glPushMatrix();
                  glTranslatef(0.0, -toe.y, 0.0);
                  renderMesh(mesh, materials[0], toe, controls);
               glPopMatrix(); // Pop back to foot position

               // [5]c. From foot translate down right to draw right toe
               glPushMatrix();
                  glTranslatef(0.45, -toe.y, 0.0);
                  glRotatef(rightToeRotation, 0, 0, 1);
                  renderMesh(mesh, materials[0], toe, controls);
               glPopMatrix(); // Pop back to foot position
            glPopMatrix(); // Pop foot / ankle joint
         glPopMatrix(); // Pop lower leg / lower/ankle leg joint
      glPopMatrix(); // Pop middle leg / middle/lower leg joint
   glPopMatrix(); // Pop upper leg / upper/middle leg joint
}

void drawArm(Mesh *mesh, Animation *animation, Material *materials[], bool left,
   DebugControls *controls)
{
   // Size scaling, Format = { wide, long, tall }
   vec3f upperArm = cVec3f(0.45, 0.7, 0.4);
   vec3f lowerArm = cVec3f(0.45, 0.7, 0.4);
   vec3f hand = cVec3f(0.5, 0.55, 0.5);
   vec3f finger = cVec3f(0.15, 0.35, 0.15);
   vec3f begin = cVec3f(1.75, -0.5, 2.0);

   float armRotation = 20;
   float upperArmRotation = 60.0;
   float lowerArmRotation = -130;
   float leftFingerRotation = -25;
   float rightFingerRotation = 25;

   float shoulderAnimation = 0;
   float elbowAnimation = 0;

   if (animation)
   {
      shoulderAnimation = animation->shoulder.rotation;
      elbowAnimation = animation->elbow.rotation;
   }

   // Rotation based left or right arm (if left = negative, right = default)
   if (left)
   {
      begin = cVec3f(-1.75, -0.5, 2.0);
      armRotation = -armRotation;
   }

   // [1]. Translate to starting shoulder joint
   glPushMatrix();
      glTranslatef(begin.x, begin.y, begin.z); // Move to shoulder
      glRotatef(armRotation, 0, 0, 1); // Rotate entire arm outwards
      glRotatef(upperArmRotation, 1, 0, 0); // Rotate upper arm backwards
      glRotatef(shoulderAnimation, 1, 0, 0); // Add rotation animation
      glTranslatef(0.0, -upperArm.y, 0.0); // Move to center of upper arm
      renderMesh(mesh, materials[0], upperArm, controls); // Draw upper arm

      // [2]. From upper arm translate down to elbow
      glPushMatrix();
         glTranslatef(0.0, -upperArm.y, 0.0); // Move to elbow
         glRotatef(lowerArmRotation, 1, 0, 0); // Rotate lower arm forwards
         glRotatef(elbowAnimation, 1, 0, 0); // Add rotation animation
         glTranslatef(0.0, -lowerArm.y, 0.0); // Move to center of lower arm
         renderMesh(mesh, materials[0], lowerArm, controls); // Draw lower arm

         // [3]. From lower arm translate down to wrist
         glPushMatrix();
            glTranslatef(0.0, -lowerArm.y, 0.0); // Move to wrist
            glTranslatef(0.0, -hand.y, 0.0); // Move to center of hand
            renderMesh(mesh, materials[0], hand, controls); // Draw hand
            glTranslatef(0.0, -hand.y, 0.0); // Move to end of hand

            // [4]a. From hand translate down and left to draw left finger
            glPushMatrix();
               glTranslatef(-0.45, -finger.y, 0.0);
               glRotatef(leftFingerRotation, 0, 0, 1);
               renderMesh(mesh, materials[0], finger, controls);;
            glPopMatrix();  // Pop back to hand position
            
            // [4]b. From hand translate down to draw middle finger
            glPushMatrix();
               glTranslatef(0.0, -finger.y, 0.0);
               renderMesh(mesh, materials[0], finger, controls);
            glPopMatrix(); // Pop back to hand position
            
            // [4]c. From hand translate down right to draw right finger
            glPushMatrix();
               glTranslatef(0.45, -finger.y, 0.0);
               glRotatef(rightFingerRotation, 0, 0, 1);
               renderMesh(mesh, materials[0], finger, controls);
            glPopMatrix(); // Pop back to hand position
         glPopMatrix(); // Pop shoulder / upper arm
      glPopMatrix(); // Pop elbow / lower arm
   glPopMatrix(); // Pop wrist / hand
}