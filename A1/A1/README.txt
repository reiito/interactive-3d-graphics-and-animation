##### Interactive 3D Graphics and Animation Assignment 1: Frogger (part 1) #####
Submission done as a pair for:
  - s3607050 - Rei Ito
  - s3429648 - Pacific Thai

[Completed requirements]
  - Projectile able to be displayed
  - Able to adjust both speed and angle
  - Able to display both tangents and normals (can be toggled)
  - Drawn circle and parabola using both parametric and Cartesian methods
  - Able to toggle between Parametric and Cartesian methods
  - Able to  double and half number of segments for both circle and parabola
  - Projectile stops when it hits the x axis
  - All key bindings have been done

[Additions]
  - Restricted speed so it cannot be negative and added a cap
  - Restricted angle so it is between 0 degrees to 180

[Notes]
 - We added an if statement to adjust the normals for the parametric parabola when
 the angle is moved from positive x-values to negative. It flips the normals and
 inverts it like a mirror image.

 - We weren't too sure whether this is required as originally when the angle is
 in the x-axis area both the tangents would flip and point downwards.
